
// Playing Cards
// Mason Brull

#include <iostream>
#include <conio.h>

using namespace std;

enum class Rank
{
	TWO = 2,
	THREE,
	FOUR,
	FIVE,
	SIX,
	SEVEN,
	EIGHT,
	TEN,
	JACK,
	QUEEN,
	KING,
	ACE
};

enum class Suit
{
	SPADES,
	DIAMONDS,
	CLUBS,
	HEARTS
};

struct Card
{
	Rank Rank;
	Suit Suit;
};

int main()
{
	Card c1;
	c1.Rank = Rank::KING;
	c1.Suit = Suit::CLUBS;


	(void)_getch();
	return 0;
}
